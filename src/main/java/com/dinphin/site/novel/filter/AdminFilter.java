package com.dinphin.site.novel.filter;

import com.dinphin.site.novel.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 14:06
 */

public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;
        User user = (User)request.getSession().getAttribute("user");
        if (user==null){
            request.setCharacterEncoding("utf-8");
            request.getRequestDispatcher("/user/error").forward(request,response);
            return;
        }
        String roleId = user.getRoleId();
        if (roleId.equals("4")){
            filterChain.doFilter(request,response);
        }else {
            request.setCharacterEncoding("utf-8");
            request.getRequestDispatcher("/user/error").forward(request,response);
        }
    }
    @Override
    public void destroy() {

    }
}
