package com.dinphin.site.novel.util;

import com.dinphin.site.novel.entity.Activity;
import com.dinphin.site.novel.entity.Book;
import com.dinphin.site.novel.repository.ActivityRepository;
import com.dinphin.site.novel.repository.BookRepository;

import com.dinphin.site.novel.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 11:06
 */

public class TimedTaskUtil {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private BookService bookService;

    @Scheduled(fixedRate = 24 * 60 * 60 * 1000)
    public void run() {
        activityRepository.deleteAll();
        List<Book> books = bookRepository.getBooksByBookIsContract(1);
        int n = books.size(), randomNum;
        for (int i = 0; i < 10; i++) {
            for (; activityRepository.count() < 10; ) {
                randomNum = (int) (Math.random() * n);
                Activity activity = new Activity();
                activity.setBookId(books.get(randomNum).getBookId());
                activityRepository.save(activity);
                System.out.println(activityRepository.count());
            }
        }
        bookService.removeBooksClick();
        bookService.removeBooksCollections();
        bookService.removeRankBook();
        bookService.removeBooksreward();
        bookService.rankBook();
        bookService.getBooksreward();
        bookService.getBooksCollections();
        bookService.getBooksClick();
    }
}
