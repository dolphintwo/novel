package com.dinphin.site.novel.config;

import com.dinphin.site.novel.filter.AdminFilter;
import com.dinphin.site.novel.filter.LoginFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 14:08
 */

@Configuration
public class CustemConfigurerAdapter {
    @Bean
    public FilterRegistrationBean authFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("AdminFilter");
        AdminFilter adminFilter = new AdminFilter();
        registrationBean.setFilter(adminFilter);
        registrationBean.setOrder(1);
        List<String> urlList = new ArrayList<String>();
        urlList.add("/moyue1");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;

    }

    @Bean
    public FilterRegistrationBean LoginFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("LoginFilter");
        LoginFilter loginFilter = new LoginFilter();
        registrationBean.setFilter(loginFilter);
        registrationBean.setOrder(2);
        List<String> urlList = new ArrayList<String>();
        urlList.add("/moyue2");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;
    }

}
