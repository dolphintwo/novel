package com.dinphin.site.novel.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 11:54
 */

public class AlipayConfig {
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016091100485691";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCtBDC3+7lQOdeffWyiJNNhZcdN7j2b7apDbWPy328WP58PsEr3jeUVkRIPNGwu8+nAM1FZb8hqJZq7yJTdbte+n7OULskLA3Yp8JmZnfu+VgYkzRkNY9855WgB207GimecxnSN5w6IuUCRP3KDFHLkldV2TvpSjgb8P76Rv3fLmF6orVen/Xp2Ji/YGNCgBvm0li5FZsSZdZ2vpUrIhT+pJhYQhPyjs3obOsDLANiOVMMX8Xf4wAAAPhFzFwHzl+i96E70+RH/4RiHQjx2RZXlzBkpayaNn+5wFTyFLv7my1oHkWkje24rkDNNp0iOTE+oFNBhHzlg51Outfc2/MAVAgMBAAECggEAItu5RRg+Wd+NheCrRm/M9s6evSDG4OedakBb5s8FfqIK1gpa0E2SXioATXfuY/dYnRS5yx8ubdRtvG/JLmPqq8lZUP2sG7/yyrhi+gROeCa0zi1UYtJAY1XJArbAxFeWUIvbZ8uprfpE+/6gf7BbBZr6iv+Wh5N97MybBJpi9G3FkRlrh9Uy0hv4d0bVlKVUYYLj8IDAwly/YHcaVBJ7XYlnMpebvjwVdJbRavm741OIn1pDJIN/mgzXNfn/jkZA08+IlUKXOXC1sZqU2W82sBeEFoRpUfdtVqf6Zk5K+YDa8tyXX+XOhP+MbMfRwrl1TPEZwbL55suxLPF+JZOnAQKBgQDfMcgTRpwvif7R3PwTAotX2qYsNuWKMwuXa+TW+rPEnbyFN287Ih+LNZ2Jaak40DtpQWjohpxta4P9oy9DzvYOPVV2vz1zdTVFlgOJKxL3ZgRHsyvRaqSIYnxV+Esc6TlMEvZkdOzRoWD2AyNc/HO9ZBOZRKkgQpNLCGahztatJQKBgQDGclbyTkm+LbV4cNmkMCfHQQTDj7GEyjHXyY8TMJqpf35CDhOKBUPT5UxyDXJ6puwPuDtszcEoElpafDPyaHSUlm+0rPJcnyVxVjZuqOEv2HUk3mPwvOik/DM1iCVjvUhy20E/OW4l1Jg3s3BT593YK9bloITDY40wu1o1ZVJsMQKBgQDUrmlLBPP70PGDJaMGslv5wZ6rbzxX7sBS8DtGJ+t5ph1ojCRwItoQG20c7hSU0MhsSiofuXYCRnumDAQUB3kspIF2AFIcXUoAHy0LXoJaQVo+wJs2mPpkBAUbHtjkmdFBjqg/nbO1JqRwiRj/mIjpph2O5YCL/yTAUScLdT/YCQKBgQCQt7eCcudCQnExiRKesuN/YoIH35V0okNg+0NUmkBf1XMM1tkW8Sn6sCBkmDvhCDPkbtUfTplLEL9Fd4gmglhqN5704p7IPuL+WQHPB2dZDgC3x1qYqBr8nlL1+fhSWSopYI2fftE902DCViBXgCL/T1EjGkFXgBTp8eciQ2atMQKBgQC0TnTJ9TWIqa1rbgCYo6NBXjfV/zgWibGaFO0HF/rwTkiB3pwzShyDs9aMdLbnj/iFJ19mYIitNqLkH5QFcPIENBunbEz76hYGbK5uwV7vHIk77UhPq9Kh/bN0WZKgZG5jX/cyxEnomNpB0Rvuz1yPhMfx8iGjJSVQOLTX1uKs5Q==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxgd53e7S95vjoF7pYtHNiqlVbq9WjZZo18/BKjOe8ZsP+6q2faEE0XgA3WkqrkDlrj6kX0H655RN45G2hJgYg7DCnSdMysZz+jr9b2nzeCPL0Ne/g54Evzp7b19F/v84pEe5RSV252lhWEoJKgimY6z0I1rNJc1QmiU+R0CurwchJGdDNV7GMyw9iIuAMIP0cnVYYE4FucOkcBaI2bQUiDixFomnHCLbeB46ljQgSPxuh7bS9zoGt/HiFv9GQADNFQfKVIqmcvC0llxldgmp03uK1DgPBDLu02JdkbAr10jbyh1JUI6y/nW1JQ3JU1o5sJC5gcrgwCif6wsBK/qGWwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://127.0.0.1:8888/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:1212/user/returnurl";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
