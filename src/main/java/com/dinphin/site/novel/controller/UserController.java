package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Login;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import java.text.ParseException;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 14:00
 */

@RequestMapping("/user")
@RestController
public class UserController extends BaseController {
    @PostMapping("/send")
    public Result sendEmail(@RequestParam("email") String email) throws MessagingException {
        String code = userService.generateShortUuid();
        userService.sendEmail(email, code);
        session.setAttribute("code", code);
        return new Result(ResultCode.SUCCESS);
    }

    @PostMapping("/register")
    public Result register(Model model, @RequestParam("userAccount")
            String userAcount, @RequestParam("email") String email, @RequestParam("phone") String phone, @RequestParam String password, @RequestParam("thirdparty") String thirday) {
        userService.register(email, phone, password, userAcount, thirday);
        session.removeAttribute("code");
        return new Result(ResultCode.SUCCESS, "注册成功");
    }


    @PostMapping("/forgetPwd")
    public Result jundgeEmail(@RequestParam("email") String email) {
        boolean jundge = userService.judgeIdentity(email);
        if (jundge) {
            return new Result(ResultCode.SUCCESS, "该邮箱存在");
        } else {
            return new Result(ResultCode.DEFEATEDEMAIL, "该邮箱不存在");
        }
    }

    @PostMapping("/judgeIdentity")
    public Result jundgeIdentity(@RequestParam("identity") String identity) {
        boolean jundge = userService.judgeIdentity(identity);
        if (jundge){
            return new Result(ResultCode.DEFEATEDEMAIL, "该账号已存在,请换个账号");
        }else
            return new Result(ResultCode.SUCCESS, "完美！可注册");
    }

    @PostMapping("/judgeEmail")
    public Result jundgeEma(@RequestParam("identity") String identity) {
        boolean jundge = userService.judgeIdentity(identity);
        if (jundge){
            return new Result(ResultCode.SUCCESS);
        }else
            return new Result(ResultCode.DEFEATEDEMAIL,"该邮箱还未被注册");
    }

    @PostMapping("/judegeEmailcode")
    public Result judegeEmailCode(@RequestParam("code") String code) {
        String code1 = (String) session.getAttribute("code");
        if (code.equals(code1)) {
            return new Result(ResultCode.SUCCESS, "验证通过");
        } else {
            return new Result(ResultCode.FAIL, "验证失败");
        }
    }

    @GetMapping(value = "/jundgeuser")
    public Result jundgeUser() {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return new Result(ResultCode.FAIL, "请先登录");
        } else {
            return new Result(ResultCode.SUCCESS, "用户已登录");
        }
    }


    @PostMapping("/update")
    public Result updateUser(User user, String usersBirthday) throws ParseException, ParseException {
        return userService.updateUser(user, usersBirthday, session);
    }

    @GetMapping("/usermessage")
    public Result userMessage() {
        return userService.userMessage(session);
    }

    @PostMapping(value = "/forgetPwdU")
    public Result addPassword(Login login) {
        String identify = login.getIdentity();
        Login login1 = loginRepository.findByIdentity(identify);
        Long id = login1.getUserId();
        loginRepository.updatePassword(login.getCertificate(), id);
        return new Result(ResultCode.SUCCESS);

    }

    @RequestMapping("/forgetPwdUI")
    public ModelAndView toLogin() {
        return new ModelAndView("forget");
    }

    @RequestMapping("/registerUI")
    public ModelAndView toRegister() {
        return new ModelAndView("register");
    }


    @RequestMapping(value = "/index")
    public ModelAndView toIndex(Model model) {
        bookService.index(model,bookService.rankBook(),bookService.getBooksreward(),bookService.getBooksClick(),bookService.getBooksCollections());
        return new ModelAndView("index");
    }
    @GetMapping("/outlogin")

    public ModelAndView outlogin(Model model) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            session.removeAttribute("user");
        }
        return new ModelAndView("redirect:/user/index");
    }

    @PostMapping("/upload/img")
    public ModelAndView uploadimg(MultipartFile file) {
        return userService.uploaadUserImg(file, session);
    }
}
