package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.service.BrowseService;
import com.dinphin.site.novel.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 13:52
 */

@Controller
@RequestMapping("/browse")
public class BrowseController extends BaseController {
    @Autowired
    private BrowseService browseService;

    //删除浏览记录
    @DeleteMapping
    public Result deleteBrowse(long browseId) {
        return browseService.deleteBrowse(browseId, session);
    }

    //获取用户主页个人浏览记录
    @GetMapping("/all")
    public ModelAndView getAllBrowse(@RequestParam("page") int page, Model model) {
        return browseService.findBrowse(page, session, model);
    }

    //获取书籍页面个人书签
    @GetMapping("/bookmarks")
    public Result getBookmarks(long bookId) {
        return browseService.findBookmark(bookId, session);
    }


}
