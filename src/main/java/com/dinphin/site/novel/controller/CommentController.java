package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Comment;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 13:54
 */

@RestController
@RequestMapping("/comment")
public class CommentController extends BaseController {
    //用户添加对书籍评论
    @PostMapping
    public Result addComment(Comment comment) {
        return commentService.addComment(comment, session);
    }

    //用户个人页面评论的展示
    @GetMapping("/user")
    public ModelAndView showByUserId(@RequestParam("page") int page, Model model) {
        return commentService.commentsByUserId(page, session, model);
    }


    //评论分页的展示
    @GetMapping("/book")
    public ModelAndView showByBookId(@RequestParam("page") int page, long bookId, Model model) {
        return commentService.commentsByBookId(page, bookId, model);
    }



    //用户删除评论
    @DeleteMapping
    public Result deleteComment(long commentId) {
        return commentService.deleteComment(commentId, session);
    }

    //用户批量删除评论
    @DeleteMapping("/book")
    public Result deleteComment(long[] num) {
        return commentService.deleteComments(num, session);
    }
}
