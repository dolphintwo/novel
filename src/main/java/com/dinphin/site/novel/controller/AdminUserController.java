package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Type;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 11:09
 */

@RestController
@RequestMapping("/admin")
public class AdminUserController extends BaseController {
    @PostMapping("/user")//查找某个用户
    public ModelAndView searchUser(Model model, User serachuser) {
        model.addAttribute("serachuser",userService.searchUser(serachuser.getUserName()));
        return new ModelAndView("usermanage");
    }

    @PutMapping("/user")//在编辑面保存用户更改的信息
    public Result saveUser(User user) {
        userRepository.save(user);
        return new Result(ResultCode.SUCCESS, "保存成功");
    }
    @DeleteMapping ("/user/{userId}")//删除一个用户
    public Result deleteUser(@PathVariable("userId") Long userId) {
        userService.deleteUser(userId);
        return new Result(ResultCode.SUCCESS);
    }
    @PostMapping ("/users")//点击删除多个用户
    public Result delateUsers(@RequestParam("usersId") Long[] usersId) {
        userService.delateUsers(usersId);
        return new Result(ResultCode.SUCCESS);
    }
    @GetMapping("/parentype")//展示父类型
    public ModelAndView parentype(Model model) {
        typeService.parentype(model);
        return new ModelAndView("booktype");
    }

    @DeleteMapping ("/parentype")//删除父类型
    public Result parentype(Model model, @RequestParam("parentypeId") String id) {
        Long parentypeId = Long.parseLong(id);
        typeRepository.deleteById(parentypeId);
        return new Result(ResultCode.SUCCESS, "删除成功");
    }

    @PutMapping ("/parentype")//增加父类型
    public Result addParentype(Model model, @RequestParam("typeName") String typeName) {
        typeService.addParentype(model,typeName);
        return new Result(ResultCode.SUCCESS, "增加成功");
    }

    @GetMapping ("/cheangtypeName")//更改父类型名字
    public Result typeName(@RequestParam("typeId") Long id, @RequestParam("typeName") String typeName) {
        typeService.typeName(id,typeName);
        return new Result(ResultCode.SUCCESS, "更改成功");
    }

    @GetMapping("/sontype")//展示子类型
    public Result sontypeView(Model model, @RequestParam("parentTypeId") Long id) {
        return new Result(ResultCode.SUCCESS, typeService.sontypeView(model,id));
    }
    @GetMapping("/updatesons")//更改类型名字 删除子类型
    public Result deletson(@RequestParam("sonId")Long[] son ,@RequestParam("sonName") String[] sonName){
        bookService.updatesons(son,sonName);
        return new Result(ResultCode.SUCCESS);
    }
    @GetMapping("/searchType")//搜索类型
    public Result psearchType(Model model, @RequestParam("typeName") String typeName) {
        Type type = typeRepository.findByTypeName(typeName);
        if (type != null) {
            model.addAttribute("type", type);
            return new Result(ResultCode.SUCCESS);
        } else {
            return new Result(ResultCode.FAIL, "没有此类型");
        }
    }

    @GetMapping("/label")//展示全部标签
    public Result lableView(Model model) {
        model.addAttribute("lables", typeService.lables());
        return new Result(ResultCode.SUCCESS);
    }

    @DeleteMapping("lable")//删除标签
    public Result deleteLable(@RequestParam("lableId") Long id) {
        typeService.deleteLable(id);
        return new Result(ResultCode.SUCCESS);

    }
    @PutMapping("/lable")//更改标签
    public Result updateLable(@RequestParam("lableId") Long id, @RequestParam("lableName") String lableName) {
        typeService.updateLable(id,lableName);
        return new Result(ResultCode.SUCCESS);
    }
    @GetMapping("/bms") //进入后台
    public ModelAndView tobms(Model model,@RequestParam("page") String currentPage){
        userService.tobms(model,currentPage);
        return new ModelAndView("bms");
    }
}