package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Login;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 13:57
 */


@RestController
@RequestMapping("/login")
public class LoginController extends BaseController {

    //站内登陆
    @PostMapping(value = "loginUI")
    public Result insideLogin(Login login) {
        return loginService.insideLogin(login, session);
    }

    //解绑账号
    @GetMapping(value = "/untie/{loginId}")
    public Result untieLogin(@PathVariable("loginId") long loginId) {
        return loginService.deleteLogin(loginId, session);
    }


    @GetMapping(value = "gitCallBack.do")
    public ModelAndView gitLogin(String code, Model model) throws IOException {
        Login login = new Login();
        String identity = loginService.githubLogin(code);
        User user = (User) session.getAttribute("user");
        return loginService.githubJudge(identity, session, "github", model);
    }

    @PostMapping("/binding/phone")
    public Result addPhone(String phone) {
        return loginService.addPhone(phone, session);
    }


    @GetMapping(value = "/security")
    public Result showLogins() {
        return loginService.showLoginMessage(session);
    }

    @PostMapping(value = "user/forgetPwdU")
    public Result addPassword(Login login) {
        System.out.println("asdfjsdfjd  " + login.getIdentity());
        String identify = login.getIdentity();
        Login login1 = loginRepository.findByIdentity(identify);
        Long id = login1.getUserId();
        System.out.println("qwnklfweqkljfeopwgj" + id);
        loginRepository.updatePassword(login.getCertificate(), id);
        return new Result(ResultCode.SUCCESS);
    }


}
