package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Book;
import com.dinphin.site.novel.service.BrowseService;
import com.dinphin.site.novel.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 13:49
 */

@RestController
@RequestMapping("/book")
public class BookController extends BaseController {
    @Autowired
    protected BrowseService browseServicek;
    @GetMapping("/chapterView/{bookId}/{listorderId}")
    public ModelAndView chapterView(Model model, @PathVariable("bookId") long bookId, @PathVariable("listorderId") int listorderId) throws FileNotFoundException {
        return bookService.chapterView(model, bookId, listorderId, session);
    }

    @GetMapping("/book")
    public ModelAndView bookView(Model model) {
        List<Book> bookList = bookRepository.findAll();
        model.addAttribute("booklist", bookList);
        return new ModelAndView("book");
    }

    @GetMapping("/bookView/{bookId}")
    public ModelAndView bookView(Model model, @PathVariable("bookId") long bookId) {
        bookService.bookView(model, bookId, session);
        model.addAttribute("comments",commentService.commentsByBook(bookId));
        return new ModelAndView("book");
    }
    @GetMapping("/subscription/{bookId}")
    public ModelAndView subscription(Model model, @PathVariable("bookId") long bookId) {
        bookService.bookView(model, bookId, session);
        return new ModelAndView("subscription");
    }
    @GetMapping("/rankinglists/{page}")
    public ModelAndView ranks(Model model, @PathVariable("page") int page) {

        List<Book> rankBookList = bookService.rankBook();
        List<Book> bookList = new ArrayList<>();
        for (int i = (page - 1) * 20; i <= (page * 20); i++) {
            bookList.add(rankBookList.get(i));
        }
        model.addAttribute("rankbook", bookList);
        return new ModelAndView("/");
    }

    @PostMapping("/keyword")//下拉框提示接口
    public Result keyword(Model model, @RequestParam("keyword") String keyWord) {
        return bookService.keyword(keyWord);
    }

    @PostMapping("/searchbook")//搜索书籍
    public ModelAndView searchsBook(Model model,@RequestParam("bookName") String bookName){
        model.addAttribute("booklist",bookService.searchBook(bookName));
        return new ModelAndView("searchs");
    }

    @RequestMapping("/seniority")
    public ModelAndView torank(Model model) {
        bookService.toIndex(model, bookService.rankBook(), bookService.getBooksreward(), bookService.getBooksClick(), bookService.getBooksCollections());
        return new ModelAndView("seniority");
    }

    @GetMapping("/booktype")
    public ModelAndView getBookByTypeId(@RequestParam("typeId") int typeId, @RequestParam("page") int page, Model model) {
        return bookService.getBooksByTypeId(page, typeId, model);
    }

    //获取限时免费书籍
    @GetMapping("/activity")
    public Result activityBook(){
        return activityService.showActivity();
    }
}
