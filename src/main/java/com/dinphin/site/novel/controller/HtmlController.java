package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Type;
import com.dinphin.site.novel.entity.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 13:55
 */


@RestController
@RequestMapping
public class HtmlController extends BaseController {
    @GetMapping("/user/myhomehtml")
    public ModelAndView myhomehtml(Model model){
        User user = (User)session.getAttribute("user");
        User currentUser =userRepository.findById(user.getUserId()).get();
        model.addAttribute("isVip",user.getRoleId());
        model.addAttribute("money",currentUser.getUserRewards());
        model.addAttribute("bookCount", bookcaseRepository.countByGroupIdAndIsShow(user.getUserId(),1));
        model.addAttribute("headUrl",user.getUserHeadPortraitUrl());
        return new ModelAndView("my-home");
    }

    @RequestMapping("/user/messagehtml")
    public ModelAndView messagehtml(){
        return new ModelAndView("message");
    }
    @RequestMapping("/user/uploadhtml")
    public ModelAndView uploadhtml(){
        return new ModelAndView("set-up");
    }
    @RequestMapping("/book/bookcaselist")
    public ModelAndView persionhtml(){
        System.out.println("======================");
        return new ModelAndView("personal");
    }
    //    财务中心
    @RequestMapping("/user/financialhtml")
    public ModelAndView financialhtml(Model model){
        consumeService.showFinancial(model,session);
        return new ModelAndView("financial");
    }
    //    安全中心-首页
    @RequestMapping("/user/safetyhtml")
    public ModelAndView safetyhtml(Model model){
        User user = (User)session.getAttribute("user");
        User currentUser =userRepository.findById(user.getUserId()).get();
        model.addAttribute("headUrl",user.getUserHeadPortraitUrl());
        return new ModelAndView("safety-home");
    }
    //    安全中心-安全工具
    @RequestMapping("/user/safetytoolhtml")
    public ModelAndView safetytoolhtml(){
        return new ModelAndView("safety-tool");
    }
    //    安全中心-密码管理
    @RequestMapping("/user/pwordhtml")
    public ModelAndView pwordhtml(){
        return new ModelAndView("pword-manager");
    }
    //    安全中心-消费设置
    @RequestMapping("/user/settinghtml")
    public ModelAndView settinghtml(){
        return new ModelAndView("consumption-setting");
    }
    //    作家专区-专区首页
    @RequestMapping("/user/writerhtml")
    public ModelAndView writerhtml(){
        return new ModelAndView("writer");
    }
    //    作家专区-作品管理
    @RequestMapping("/user/foundhtml")
    public ModelAndView foundhtml(){
        return new ModelAndView("found");
    }
    //    作家专区-创建作品
    @RequestMapping("/user/workshtml")
    public ModelAndView workshtml(Model model){
        List<Type> typeList = typeRepository.findByTypeParentId((long) 0);
        List<Type> labelList = typeRepository.findByTypeParentId((long) -1);
        model.addAttribute("typelist", typeList);
        model.addAttribute("label", labelList);
        return new ModelAndView("works");
    }
    //我的书单
    @RequestMapping("/user/booktaghtml")
    public ModelAndView booktaghtml(Model model){
        User user = (User) session.getAttribute("user");
        bookcaseService.bookcaseView(user,model);
        return new ModelAndView("book-list");
    }
    //   全部作品
    @RequestMapping("/user/allwork")
    public ModelAndView allwork(Model model){
        model.addAttribute("bookList",bookRepository.findAll());
        return new ModelAndView("allwork");
    }
    //    完本
    @RequestMapping("/user/finish")
    public ModelAndView finish(Model model){
        model.addAttribute("finshBook",bookService.finshedBook());
        return new ModelAndView("finish");
    }
    @RequestMapping("/user/unfinish")
    public ModelAndView nufinish(Model model){
        model.addAttribute("unfinshBook",bookService.unfinshedBook());
        return new ModelAndView("finish");
    }
    //    免费
    @RequestMapping("/user/free")
    public ModelAndView free(Model model){
        model.addAttribute("free",bookService.freeBook());
        return new ModelAndView("free");
    }
    //   非 免费
    @RequestMapping("/user/unfree")
    public ModelAndView unfree(Model model){
        model.addAttribute("unfree",bookService.unfreeBook());
        return new ModelAndView("unfree");
    }
    @RequestMapping("/user/managelogin")
    public ModelAndView managelogin(){
        return new ModelAndView("managelogin");
    }
}
