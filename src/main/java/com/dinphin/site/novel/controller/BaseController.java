package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.repository.*;
import com.dinphin.site.novel.service.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 11:13
 */

public class BaseController {
    @Autowired
    protected UserService userService;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected LoginRepository loginRepository;
    @Autowired
    protected LoginService loginService;
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected HttpSession session;
    @Autowired
    protected BookRepository bookRepository;
    @Autowired
    protected BookService bookService;
    @Autowired
    protected BookLinkTypeService bookLinkTypeService;
    @Autowired
    protected SensitiveWordService sensitiveWordService;
    @Autowired
    protected CommentService commentService;
    @Autowired
    protected BookListService bookListService;
    @Autowired
    protected BookLinkTypeRepository bookLinkTypeRepository;
    @Autowired
    protected TypeRepository typeRepository;
    @Autowired
    protected BookListRepository bookListRepository;
    @Autowired
    protected BookcaseRepository bookcaseRepository;
    @Autowired
    protected BookcaseService bookcaseService;
    @Autowired
    protected ConsumeService consumeService;
    @Autowired
    protected ConsumeRepository consumeRepository;
    @Autowired
    protected MessageService messageService;
    @Autowired
    protected TypeService typeService;
    @Autowired
    protected ActivityService activityService;
    @Autowired
    protected BrowseService browseServicek;
}
