package com.dinphin.site.novel.controller;

import com.dinphin.site.novel.entity.Book;
import com.dinphin.site.novel.entity.BookList;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/6 11:17
 */

@RestController
@RequestMapping("/author")
public class AuthorController extends BaseController {
    //获得书籍详情
    @GetMapping("/message/{bookId}")
    public Result bookMessage(@PathVariable("bookId") long bookId) {
        return new Result(ResultCode.SUCCESS, bookService.getBookMessage(bookId));
    }

    //作者写作
    @PostMapping("/write")
    public Result writeBook(BookList bookList, String content) {
        if (bookService.writeBook(bookList, content)) return new Result(ResultCode.SUCCESS, "章节上传成功!");
        else return new Result(ResultCode.FAIL, "章节上传失败，请重试！");
    }

    //书籍创建
    @PostMapping("/add")
    public Result addBook(MultipartFile file, Book book, int[] types) {
        bookService.addBook(file, book, types);
        return new Result(ResultCode.SUCCESS, "书籍已成功创建！");
    }

    //创作页面获得新章节指定信息
    @GetMapping("/writehtml/{bookId}")
    public Result writeBookHtml(@PathVariable("bookId") long bookId) {
        return bookService.getBooklist(bookId);
    }

    @GetMapping("/build")
    public Result tuAuthor() {
        User user = (User) session.getAttribute("user");
        user.setRoleId("2");
        userRepository.save(user);
        return new Result(ResultCode.SUCCESS);
    }
}
