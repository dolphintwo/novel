package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.BookList;
import com.dinphin.site.novel.repository.BookListRepository;
import com.dinphin.site.novel.service.BookListService;
import com.dinphin.site.novel.util.IoUtil;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:33
 */

@Service
public class BookListServiceImpl implements BookListService {
    @Autowired
    private BookListRepository bookListRepository;

    public Result deleteBookList(long id) {
        BookList bookList = bookListRepository.getOne(id);
        bookListRepository.deleteById(id);
        new IoUtil().deleteFile("." + bookList.getBookListUrl());
        return new Result(ResultCode.SUCCESS);
    }

    public Result updateBookList(BookList bookList) {
        bookListRepository.save(bookList);
        return new Result(ResultCode.SUCCESS);
    }
}
