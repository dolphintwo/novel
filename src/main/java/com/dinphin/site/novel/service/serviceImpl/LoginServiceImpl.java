package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.Login;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.repository.LoginRepository;
import com.dinphin.site.novel.repository.UserRepository;
import com.dinphin.site.novel.service.LoginService;
import com.dinphin.site.novel.util.AuthUtils;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:34
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private UserRepository userRepository;

    public Result insideLogin(Login login, HttpSession session) {
        Login login1 = loginRepository.getLoginByIdentityAndCertificate(login.getIdentity(), login.getCertificate());
        if (login1 != null) {
            User user = userRepository.findById(login1.getUserId()).get();
            session.setAttribute("user", user);
            if (user.getRoleId().equals("4"))return new Result(ResultCode.ADMIN);
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    public Result deleteLogin(long loginId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (loginRepository.existsById(loginId) && loginRepository.findById(loginId).get().getUserId() == user.getUserId()) {
            loginRepository.deleteById(loginId);
            return new Result(ResultCode.SUCCESS);
        } else return new Result(ResultCode.FAIL);
    }


    public Result addPhone(String phone, HttpSession session) {
        User user = (User) session.getAttribute("user");
        Login login = loginRepository.getLoginByUserIdAndLoginType(user.getUserId(), "phone");
        if (login != null) {
            login.setIdentity(phone);
            loginRepository.save(login);
        } else {
            login.setUserId(user.getUserId());
            login.setLoginType("phone");
            login.setIdentity(phone);
            loginRepository.save(login);
        }
        return new Result(ResultCode.SUCCESS);
    }


    public String githubLogin(String code) throws IOException {
        String url = "https://github.com/login/oauth/access_token?" +
                "client_id=" + AuthUtils.CLIENT_ID +
                "&client_secret=" + AuthUtils.CLIENT_SECRET +
                "&code=" + code;
        String result = AuthUtils.doGetStr(url);
        String access_token = result.split("&")[0];
        String infoUrl = "https://api.github.com/user?" + access_token;
        JSONObject userInfo = AuthUtils.doGetJson(infoUrl);
        return userInfo.getString("id");
    }

    public ModelAndView githubJudge(String identity, HttpSession session, String loginType, Model model) {
        User user = (User) session.getAttribute("user");
        Login login = loginRepository.findByIdentity(identity);
        if (identity == null) {
            return new ModelAndView("redirect:/user/index");}
        else if (login != null && user == null) {
            this.insideLogin(login,session);
            return new ModelAndView("redirect:/user/index");
        } else if (login == null && user != null) {
            Login login1 = new Login(loginRepository.count(), user.getUserId(), loginType, identity, null);
            loginRepository.save(login1);
            model.addAttribute("message", "绑定成功！");
            return new ModelAndView("safety-home");
        } else if (login == null && user == null) {
            model.addAttribute("github", identity);
            return new ModelAndView("redirect:/user/registerUI");
        } else{
            return new ModelAndView("redirect:/user/index");}
    }

    public Result showLoginMessage(HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Login> logins = loginRepository.findByUserId(user.getUserId());
        return new Result(ResultCode.SUCCESS, logins);
    }
}
