package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:17
 */

public interface ConsumeService {
    String reward(User user, Long bookId, int money);

    String buychapters(Long[] chapters, Long userId);

    boolean isBuyed(long chapterId, long bookId, HttpSession session);

    void showFinancial(Model model, HttpSession session);

    Result jundgeReward(String virtualCurrency, Long bookId, HttpSession session);//用户进行打赏的方法

    void deleteConsume(Long consumeId);
    //批量删除消费记录
    void deleteConsumes(Long[] consumesId);
}
