package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.SensitiveWord;
import com.dinphin.site.novel.repository.SensitiveWordRepository;
import com.dinphin.site.novel.service.SensitiveWordService;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:35
 */

@Service
public class SensitiveWordServiceImpl implements SensitiveWordService{
    @Autowired
    private SensitiveWordRepository sensitiveWordRepository;

    public Result addSensitiveWord(String word) {
        SensitiveWord sensitiveWord = new SensitiveWord();
        sensitiveWord.setSensitiveWord(word);
        sensitiveWordRepository.save(sensitiveWord);
        return new Result(ResultCode.SUCCESS);

    }

    public Result deleteSensitiveWord(long wordId) {
        sensitiveWordRepository.deleteByWordId(wordId);
        return new Result(ResultCode.SUCCESS);
    }

    public ModelAndView showAllSensitiveWords(String pageNum, Model model) {
        int page;
        if (pageNum == null) page = 1;
        else page = Integer.parseInt(pageNum);
        Pageable pageable = new PageRequest(page - 1, 20);
        Page<SensitiveWord> wordPage = sensitiveWordRepository.findAll(pageable);
        model.addAttribute("page", wordPage.getNumber() + 1);
        model.addAttribute("pages", wordPage.getTotalPages());
        model.addAttribute("words", wordPage.getContent());
        System.out.println("内容为：" + wordPage.getContent());
        return new ModelAndView("sensitive");

    }

    public Result updateSensitiveWord(SensitiveWord sensitiveWord) {
        sensitiveWordRepository.save(sensitiveWord);
        return new Result(ResultCode.SUCCESS);
    }


    public Result mateWord(String word) {
        List<SensitiveWord> sensitiveWords = sensitiveWordRepository.findAll();
        for (int i = 0; i < sensitiveWords.size(); i++) {
            String pattern = sensitiveWords.get(i).getSensitiveWord();
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(word);
            if (m.find()) return new Result(ResultCode.FAIL);
        }
        return new Result(ResultCode.SUCCESS);
    }
}
