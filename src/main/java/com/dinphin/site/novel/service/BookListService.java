package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.BookList;
import com.dinphin.site.novel.util.Result;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:16
 */

public interface BookListService {
    //删除章节
    Result deleteBookList(long id);

    //更新目录
    Result updateBookList(BookList bookList);
}
