package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.Login;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:17
 */

public interface LoginService {
    //登录返回TRUE,没有返回FALSE
    Result insideLogin(Login login, HttpSession session);

    //解绑账号
    Result deleteLogin(long loginId, HttpSession session);

    //绑定手机号
    Result addPhone(String phone, HttpSession session);

    //获取github账号信息
    String githubLogin(String code) throws IOException;

    //判断属于登录，添加，绑定，或者注册
    ModelAndView githubJudge(String identity, HttpSession session, String loginType, Model model);

    //显示账号信息
    Result showLoginMessage(HttpSession session);
}
