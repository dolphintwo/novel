package com.dinphin.site.novel.service;

import com.dinphin.site.novel.util.Result;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:15
 */

public interface BookLinkTypeService {
    //添加类型和书籍多对多关系
    boolean addBookLinkType(long bookId, int[] types);

    //根据类型Id查找书籍
    Result findBooksByTypes(List<Long> types);
}
