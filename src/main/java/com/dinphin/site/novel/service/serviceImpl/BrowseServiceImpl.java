package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.*;
import com.dinphin.site.novel.repository.*;
import com.dinphin.site.novel.service.BrowseService;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.*;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:33
 */

@Service
public class BrowseServiceImpl implements BrowseService {
    @Autowired
    private BrowseRepository browseRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookListRepository bookListRepository;
    @Autowired
    private BookLinkTypeRepository bookLinkTypeRepository;
    @Autowired
    private TypeRepository typeRepository;

    @Override
    public boolean addBrowse(long booklistId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        BookList bookList = bookListRepository.getOne(booklistId);
        Book book = bookRepository.getOne(bookList.getBookId());
        BookLinkType bookLinkType = bookLinkTypeRepository.findByBookId(book.getBookId()).get(0);
        Type type = typeRepository.getOne(bookLinkType.getTypeId());
        Browse browse = new Browse(0, user.getUserId(), book.getAuthorId(), book.getAuthorName(), book.getBookId(), book.getBookName(), booklistId, bookList.getBookListOrder(), bookList.getBookListName(), type.getTypeId(), type.getTypeName(), new Date(), 0, 0);
        browseRepository.save(browse);
        return true;
    }

    public Result deleteBrowse(long browseId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (browseRepository.findById(browseId).get().getUserId() == user.getUserId()) {
            browseRepository.deleteById(browseId);
            return new Result(ResultCode.SUCCESS);
        } else return new Result(ResultCode.FAIL);
    }

    public Result deleteBrowwses(long[] browseIds, HttpSession session) {
        User user = (User) session.getAttribute("user");
        for (int i = 0; i < browseIds.length; i++) {

            browseRepository.deleteById(browseIds[i]);
        }
        return new Result(ResultCode.SUCCESS);
    }

    public Result findBookmark(long bookId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Browse> browses = browseRepository.getBrowsesByBookIdAndUserIdAndIsBookmarks(bookId, user.getUserId(), 1);
        if (browses.size() != 0) return new Result(ResultCode.SUCCESS, browses);
        else return new Result(ResultCode.FAIL, "还没有书签，快去动动手吧！");
    }

    public ModelAndView findBrowse(int page, HttpSession session, Model model) {
        User user = (User) session.getAttribute("user");
        Pageable pageable = new PageRequest(page - 1, 10);
        Specification<Browse> specification = new Specification<Browse>() {
            @Override
            public Predicate toPredicate(Root<Browse> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path path = root.get("userId");
                return criteriaBuilder.equal(path, user.getUserId());
            }
        };
        Page<Browse> browsePage = browseRepository.findAll(specification, pageable);
        model.addAttribute("page", browsePage.getNumber() + 1);
        model.addAttribute("pages", browsePage.getTotalPages());
        model.addAttribute("browses", browsePage.getContent());
        return new ModelAndView("");
    }
}
