package com.dinphin.site.novel.service;

import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:17
 */

public interface BrowseService {
    //添加浏览记录
    boolean addBrowse(long booklistId,HttpSession session);

    //删除浏览记录
    Result deleteBrowse(long browseId, HttpSession session);

    //批量删除浏览记录
    Result deleteBrowwses(long[] browseIds, HttpSession session);

    //显示用户页浏览记录
    ModelAndView findBrowse(int page, HttpSession session, Model model);

    //显示书签
    Result findBookmark(long bookId, HttpSession session);
}
