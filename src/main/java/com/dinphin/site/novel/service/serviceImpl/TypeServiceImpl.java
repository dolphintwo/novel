package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.Type;
import com.dinphin.site.novel.repository.BookRepository;
import com.dinphin.site.novel.repository.TypeRepository;
import com.dinphin.site.novel.service.BookService;
import com.dinphin.site.novel.service.TypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;


/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:35
 */
@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private BookService bookService;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private BookRepository bookRepository;
    @Override
    public void parentype(Model model){
        List<Type> parenttypes = bookService.findtypeId((long) 0);

        model.addAttribute("parenttypes", parenttypes);
    }
    @Override
    public void addParentype(Model model,String parentypeName){
        Type type = new Type();
        type.setTypeName(parentypeName);
        type.setTypeParentId((long) 0);
        typeRepository.save(type);
        List<Type> parenttypes = bookService.findtypeId((long) 0);
        model.addAttribute("parenttypes", parenttypes);
    }
    @Override
    public void typeName( Long id,String typeName){
        Type type = typeRepository.findById(id).get();
        type.setTypeName(typeName);
        typeRepository.save(type);
    }
    @Override
    public List<Type> sontypeView(Model model, Long id){
        List<Type> sontypes =typeRepository.findByTypeParentId(id);
        return sontypes;
    }
    @Override
    public  List<Type> lables(){
        List<Type> lables = typeRepository.findByTypeParentId((long) -1);
        return lables;
    }
    @Override
    public void deleteLable(Long id){
        typeRepository.deleteById(id);
    }
    @Override
    public void updateLable(Long id,String lableName){
        Type type = typeRepository.findById(id).get();
        type.setTypeName(lableName);
        type.setTypeParentId((long) -1);
        typeRepository.save(type);
        List<Type> lables = typeRepository.findByTypeParentId((long) -1);
    }
}
