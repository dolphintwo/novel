package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:16
 */

public interface BookcaseService {
    boolean saveBookCase(Long userId, Long bookId);

    void bookcaseView(User user, Model model);

    Result addBookCase(User user, long bookId);

    void bookCaseList(Model model, User user);

    void deleteBookCase(Model model, User user, Long id);
}
