package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.*;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:16
 */

public interface BookService {
    //模糊查询，分页显示
    ModelAndView getBookSByBookName(String page,String fbookName,Model model);

    //写书页面出传入章节信息
    Result getBooklist(long boookId);

    //类型Id分页显示书籍
    ModelAndView getBooksByTypeId(int page, int typeId, Model model);

    //分页显示所有书籍
    ModelAndView getAllBook(int page, Model model);

    //添加书籍
    Result addBook(MultipartFile file, Book book, int[] types);

    //更新书籍信息
    Result updateBook(Book book);

    //书的Id删除书籍
    Result deleteBook(long bookId);

    //批量删除书籍
    Result deleteBooks(long[] num);

    //作者网页创作
    boolean writeBook(BookList bookList, String context);

    //上传书籍
    Result uploadBook(MultipartFile bookfile, MultipartFile imgfile, Book book, User user, int[] types);

    //根据多对多查询书籍
    List<Book> getBooksByBookLinkTypes(List<BookLinkType> bookLinkTypes);

    List<String> paragraphList(String path) throws FileNotFoundException;

    List<Type> findtypeId(Long typeId);

    List<Book> rankBook();

    Map<Long, Integer> mapSort(Map unsortMap);


    List<String> getKeyWord(String keyWord);

    void index(Model model, List<Book> ranBook, List<Book> Booksreward, List<Book> BooksClick, List<Book> BooksCollections);

    void toIndex(Model model, List<Book> ranBook, List<Book> Booksreward, List<Book> BooksClick, List<Book> BooksCollections);

    List<Book> sub(int end, List<Book> list);

    List<Book> getBooksreward();

    List<Book> getBooksClick();

    List<Book> getBooksCollections();

    ModelAndView chapterView(Model model, long bookId, int listorderId, HttpSession session) throws FileNotFoundException;

    void bookView(Model model, long bookId, HttpSession session);

    //自动填充默认信息
    Book getDefault(Book book);

    List<Book> getBooksByType(long typeId);

    void updatesons(Long[] son, String[] sonName);

    Result keyword(String keyword);

    Result getBookMessage(long bookId);

    void removeBooksreward();

    void removeBooksClick();

    void removeBooksCollections();

    void removeRankBook();

    List<Book> searchBook(String bookName);

    List<Book> recommend(Long userId, List<Book> rankList);


    List<Book> finshedBook();

    void finshBook();

    List<Book> unfinshedBook();

    void unfinshBook();

    List<Book> unfreeBook();

    void unfreedBook();

    List<Book> freeBook();

    void freedBook();
}
