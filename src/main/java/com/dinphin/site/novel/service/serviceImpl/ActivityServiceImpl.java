package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.Activity;
import com.dinphin.site.novel.entity.Book;
import com.dinphin.site.novel.repository.ActivityRepository;
import com.dinphin.site.novel.repository.BookRepository;
import com.dinphin.site.novel.service.ActivityService;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:06
 */

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private BookRepository bookRepository;

    @Override
    public Result showActivity() {
        List<Activity> activities = activityRepository.findAll();
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < activities.size(); i++) {
            books.add(bookRepository.findById(activities.get(i).getBookId()).get());
        }
        return new Result(ResultCode.SUCCESS, books);
    }
}
