package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.Comment;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.repository.CommentRepository;
import com.dinphin.site.novel.service.CommentService;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.*;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:34
 */

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public Result addComment(Comment comment, HttpSession session) {
        comment.setUserId(((User) session.getAttribute("user")).getUserId());
        comment.setCommentDate(new Date());
        commentRepository.save(comment);
        return new Result(ResultCode.SUCCESS, "评论成功！");
    }

    public Result deleteComment(long id, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (commentRepository.getOne(id).getUserId() == user.getUserId()) {
            commentRepository.deleteById(id);
            return new Result(ResultCode.SUCCESS, "删除成功！");
        } else return new Result(ResultCode.FAIL);
    }

    @Override
    public Result deleteComments(long[] nums, HttpSession session) {
        User user = (User) session.getAttribute("user");
        for (long commentId : nums) {
            if (commentRepository.getOne(commentId).getUserId() == user.getUserId()) commentRepository.deleteById(commentId);
            return new Result(ResultCode.FAIL);
        }
        return new Result(ResultCode.SUCCESS);
    }

    @Override
    public List<Comment> commentsByBook(long bookId) {
        Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "commentDate"));
        Pageable pageable = new PageRequest(0, 10, sort);
        Specification<Comment> specification = new Specification<Comment>() {
            @Override
            public Predicate toPredicate(Root<Comment> root,
                                         CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder criteriaBuilder) {
                Path path = root.get("bookId");
                criteriaBuilder.gt(path, bookId);
                return criteriaBuilder.equal(path, bookId);
            }
        };
        Page<Comment> commentPage = commentRepository.findAll(specification, pageable);
        return commentPage.getContent();
    }

    public ModelAndView commentsByUserId(int page, HttpSession session, Model model) {
        User user = (User) session.getAttribute("user");
        Pageable pageable = new PageRequest(page - 1, 10);
        Specification<Comment> specification = new Specification<Comment>() {
            @Override
            public Predicate toPredicate(Root<Comment> root,
                                         CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder criteriaBuilder) {
                Path path = root.get("userId");
                criteriaBuilder.gt(path, user.getUserId());
                return criteriaBuilder.equal(path, user.getUserId());
            }
        };
        Page<Comment> commentPage = commentRepository.findAll(specification, pageable);
        model.addAttribute("page", commentPage.getNumber() + 1);
        model.addAttribute("pages", commentPage.getTotalPages());
        model.addAttribute("comments", commentPage.getContent());
        return new ModelAndView("");
    }

    public ModelAndView commentsByBookId(int page, long bookId, Model model) {
        Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "commentDate"));
        Pageable pageable = new PageRequest(page - 1, 10, sort);
        Specification<Comment> specification = new Specification<Comment>() {
            @Override
            public Predicate toPredicate(Root<Comment> root,
                                         CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder criteriaBuilder) {
                Path path = root.get("bookId");
                criteriaBuilder.gt(path, bookId);
                return criteriaBuilder.equal(path, bookId);
            }
        };
        Page<Comment> commentPage = commentRepository.findAll(specification, pageable);
        model.addAttribute("page",commentPage.getNumber()+1);
        model.addAttribute("pages",commentPage.getTotalPages());
        model.addAttribute("comments",commentPage.getContent());
        return new ModelAndView("");
    }
}
