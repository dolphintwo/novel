package com.dinphin.site.novel.service.serviceImpl;

import com.dinphin.site.novel.entity.Book;
import com.dinphin.site.novel.entity.Bookcase;
import com.dinphin.site.novel.entity.User;
import com.dinphin.site.novel.repository.BookcaseRepository;
import com.dinphin.site.novel.repository.BookRepository;
import com.dinphin.site.novel.service.BookcaseService;
import com.dinphin.site.novel.util.Result;
import com.dinphin.site.novel.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 16:32
 */

@Service
public class BookcaseServiceImpl implements BookcaseService {
    @Autowired
    BookcaseRepository bookcaseRepository;
    @Autowired
    BookRepository bookRepository;
    @Override
    public boolean saveBookCase(Long userId, Long bookId) {
        Bookcase userBookcase = bookcaseRepository.findByBookIdAndGroupId(bookId, userId);

        if (userBookcase == null) {
            Book book = bookRepository.findById(bookId).get();
            Bookcase bookcase = new Bookcase();
            bookcase.setBookId(bookId);
            bookcase.setGroupId(userId);
            bookcase.setIsShow(1);
            int collection = book.getBookCollections();
            book.setBookCollections(collection + 1);
            bookRepository.save(book);
            bookcaseRepository.save(bookcase);
            return true;
        }
        if (userBookcase != null) {
            if (userBookcase.getIsShow() != 1) {
                userBookcase.setIsShow(1);
                bookcaseRepository.save(userBookcase);
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public void bookcaseView(User user, Model model) {
        Book book = new Book();


        List<Bookcase> bookView = bookcaseRepository.findByGroupId(user.getUserId());
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase : bookView
                ) {
            book = bookRepository.findById(bookcase.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("bookview", bookList);
    }
    @Override
    public Result addBookCase(User user,long bookId){
        Long userId = user.getUserId();
        boolean jundge = saveBookCase(userId, bookId);
        if (jundge) {
            return new Result(ResultCode.SUCCESS, "加入书架成功");
        } else {
            return new Result(ResultCode.FAIL, "该书已在书架中");
        }
    }
    @Override
    public  void bookCaseList(Model model,User user){
        Book book = new Book();
        List<Bookcase> bookView = bookcaseRepository.findByGroupIdAndIsShow(user.getUserId(),1);
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase:bookView
                ) {
            book = bookRepository.findById(bookcase.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("booklist",bookList);
    }
    @Override
     public void deleteBookCase(Model model,User user,Long bookId){
        Long userId = user.getUserId();
        Bookcase bookcase = bookcaseRepository.findByBookIdAndGroupId(bookId,userId);
        if (bookcase==null){
            return;
        }
        bookcase.setIsShow(0);
        bookcaseRepository.save(bookcase);
        System.out.println(bookcase);
        Book book = new Book();
        List<Bookcase> bookView = bookcaseRepository.findByGroupIdAndIsShow(userId, 1);
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase1 : bookView
                ) {
            book = bookRepository.findById(bookcase1.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("booklist", bookList);
    }
}
