package com.dinphin.site.novel.service;

import com.dinphin.site.novel.entity.Message;
import com.dinphin.site.novel.util.Result;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 15:18
 */

public interface MessageService {
    //新建审核信息
    Result addMessage(Message message, HttpSession session);

    //单方面删除信息
    Result deleteMessage(long id);

    //阅读信息
    ModelAndView readMessage(long id, Model model);

    //批量删除信息
    Result deleteMessages(long[] nums);

    //分页展示审核信息
    ModelAndView allMessage(int page, Model model, HttpSession session, int isBrowse);

    //对信息审核结果的修改
    Result changeMessage(long messageId, int audit, String comtent);
}
