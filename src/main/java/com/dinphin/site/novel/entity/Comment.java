package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Desc:
 * 评论记录表
 * <p>
 * Created by dd on 2018/9/5 14:31
 */

@Data
@Entity
@Table(name = "t_comment")
@NoArgsConstructor
@AllArgsConstructor
public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long commentId;                        //评论ID
    private long userId;                           //用户ID
    private long bookId;                           //书籍ID
    private String commentContent;                 //评论内容
    private Date commentDate;                      //评论时间
}
