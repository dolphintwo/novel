package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
/**
 * Desc:
 * 书籍与分类 多对多关系表
 *
 * Created by dd on 2018/9/5 14:16
 */

@Data
@Entity
@Table(name = "t_booklinktype")
@NoArgsConstructor
@AllArgsConstructor
public class BookLinkType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;                         //ID
    private long bookId;                     //书籍ID
    private long typeId;                     //书籍所属分类的ID
}
