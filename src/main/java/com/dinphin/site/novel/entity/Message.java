package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Desc:
 * 反馈信息表
 * <p>
 * Created by dd on 2018/9/5 14:33
 */

@Data
@Entity
@Table(name = "t_message")
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long messageId;                    //信息ID
    private long userId;                       //用户ID
    private long bookId;                       //书籍ID
    private String message;                    //信息内容
    private Date messageDate;                  //反馈时间
    private int isBrowse;                      //是否阅读(是--0，否--1)
    private int roleId;                        //信息发送者角色ID，可用于信息对不同用户显示
}
