package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Desc:
 * 敏感词汇管理表
 * <p>
 * Created by dd on 2018/9/5 14:35
 */

@Data
@Entity
@Table(name = "t_sensitiveword")
@NoArgsConstructor
@AllArgsConstructor
public class SensitiveWord implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long wordId;                         //敏感词ID
    private String sensitiveWord;                //敏感词
}
