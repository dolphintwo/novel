package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Desc:
 * 用户表
 * <p>
 * Created by dd on 2018/9/5 14:36
 */

@Table(name = "t_user")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;                         //用户ID
    private String userName;                     //读者昵称；作者笔名；管理员昵称
    private String userSex;                      //性别
    private Date userBirthday;                   //用户生日
    private String userHeadPortraitUrl;          //用户头像路径
    private Long userRewards;                    //用户虚拟币
    private String roleId;                       //角色ID
    private String description;                 //用户简介

}
