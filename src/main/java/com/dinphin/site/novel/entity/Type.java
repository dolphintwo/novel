package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Desc:
 * 分类管理表
 * <p>
 * Created by dd on 2018/9/5 14:36
 */

@Data
@Table(name = "t_booktype")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Type implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long typeId;                          //类型ID/标签ID
    private String typeName;                      //类型名字/标签名字
    private Long typeParentId;                    //类型父ID/标签父ID为-1
}
