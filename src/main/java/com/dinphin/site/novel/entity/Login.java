package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Desc:
 * 登录表
 * <p>
 * Created by dd on 2018/9/5 14:32
 */

@Data
@Entity
@Table(name = "t_login")
@NoArgsConstructor
@AllArgsConstructor
public class Login implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long loginId;                                //主键
    private long userId;                                 //用户ID
    private String loginType;                            //身份类型(phone;email;wechat;qq;github)
    private String identity;                             //身份标识(手机号，邮箱，账号或第三方账号ID）
    private String certificate;                          //密码凭证(账号登录-密码；第三方登录为空)
}
