package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:25
 */

@Data
@Entity
@Table(name = "t_booklist")
@NoArgsConstructor
@AllArgsConstructor
public class BookList implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bookListId;                          //书目录ID
    private String bookListName;                      //章节名字
    private long bookId;                              //所属书籍ID
    private String bookListUrl;                       //书籍章节URL
    private int bookListOrder;                        //目录顺序
    private int chapterPrice;                         //章节价格
    private int bookListSize;                         //字数
}