package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Desc:
 * 浏览记录表
 * <p>
 * Created by dd on 2018/9/5 14:29
 */

@Data
@Entity
@Table(name = "t_browse")
@NoArgsConstructor
@AllArgsConstructor
public class Browse implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long browseId;                      //浏览记录ID
    private long userId;                        //用户ID
    private long authorId;                      //作者ID
    private String authorName;                  //作者名字
    private long bookId;                        //书籍ID
    private String bookName;                    //书籍名字
    private long bookListId;                    //章节ID
    private int bookListOrder;                  //第几章
    private String bookListName;                //书签名字即章节名字
    private long typeId;                        //类型Id
    private String typeName;                    //类型名字
    private Date browseDate;                    //浏览日期
    private int isBookmarks;                    //是否书签
    private int isUpdate;                       //是否更新
}
