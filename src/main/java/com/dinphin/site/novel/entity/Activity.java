package com.dinphin.site.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Desc:
 * 活动管理表
 * <p>
 * Created by dd on 2018/9/5 14:24
 */

@Data
@Entity
@Table(name = "t_activity")
@NoArgsConstructor
@AllArgsConstructor
public class Activity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long activityId;                         //参加活动ID·
    private long bookId;                             //书籍ID
}
