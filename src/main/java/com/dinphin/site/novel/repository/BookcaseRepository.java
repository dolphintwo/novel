package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Bookcase;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:47
 */

public interface BookcaseRepository extends JpaRepository<Bookcase,Long> {
    Bookcase findByBookIdAndGroupId(Long BookId, Long UserId);
    List<Bookcase> findByGroupIdAndIsShow(Long userId, int isshow);
    List<Bookcase> findByGroupId(Long userId);
    int countByGroupIdAndIsShow(Long userId,int isshow);
    @Modifying
    @Query("select  o.bookId  from  Bookcase o where o.groupId=?1 and o.isShow=?2")
    List<Long> getBooksId(Long userId,int isshow);
}
