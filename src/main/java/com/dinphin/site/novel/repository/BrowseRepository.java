package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Browse;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:49
 */

public interface BrowseRepository extends JpaRepository<Browse,Long>,JpaSpecificationExecutor<Browse>{
    List<Browse> findBrowsesByUserId(long userId);
    List<Browse> getBrowsesByBookIdAndUserIdAndIsBookmarks(long bookId,long userId,int isBookarks);
}
