package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.BookLinkType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:46
 */

public interface BookLinkTypeRepository extends JpaRepository<BookLinkType, Long>,JpaSpecificationExecutor<BookLinkType>  {
    void deleteByBookId(long bookId);

    List<BookLinkType> findBookLinkTypesByTypeId(long typeId);

    List<BookLinkType> findByBookId(Long bookId);

    @Query("select  o.typeId  from  BookLinkType o where o.bookId=?1")
    List<Long> getTypeIds(Long bookId);

    List<BookLinkType> findByTypeId(Long typeId);

    @Query("select  o.bookId  from  BookLinkType o where o.typeId=?1")
    List<Long> getBookIds(Long bookId);

}
