package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.BookList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:47
 */

public interface BookListRepository extends JpaRepository<BookList, Long> {
    List<BookList> getBookListsByBookId(long bookId);

    List<BookList> findByBookIdOrderByBookListOrderAsc(Long bookId);
    BookList findByBookIdAndBookListOrder(Long bookId, int listOrder);

    BookList findByBookListId(Long bookListId);

    int countByBookId(Long bookId);
}
