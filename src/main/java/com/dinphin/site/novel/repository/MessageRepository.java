package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:50
 */

public interface MessageRepository extends JpaRepository<Message, Long>,JpaSpecificationExecutor<Message> {
    //根据用户Id获取审核信息表
    List<Message> getMessagesByUserId(Long userId);

    //根据阅读状态和用户ID获取信息表
    List<Message> getMessagesByUserIdAndIsBrowse(long userId, int isBrowse);

}
