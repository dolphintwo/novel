package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:51
 */

public interface UserRepository extends JpaRepository<User, Long> {

    User getUserByUserId(long userId);

    User findByUserName(String username);
}
