package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:40
 */

public interface BookRepository extends JpaRepository<Book, Long>,JpaSpecificationExecutor<Book>{

    List<Book> getBooksByBookNameLike(String bookName);

    Book findBookByBookName(String bookName);

    List<Book> findByBookNameLike(String keyWord);

    List<Book> findAllByOrderByBookRewardsDesc();

    List<Book> findAllByOrderByBookClicksDesc();

    List<Book> findAllByOrderByBookCollectionsDesc();

    List<Book> findAllByBookNameContaining(String bookName);

    List<Book> getBooksByBookIsContract(int bookIsContract);

    Book getBookByBookName(String bookName);

    List<Book> findAllByAuthorNameContaining(String authorName);
    List<Book> findAllByBookIsEnd(int isEnd);

    List<Book> findAllByBookIsContract(int isFree);

}
