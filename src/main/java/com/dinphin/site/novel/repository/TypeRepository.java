package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Type;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:50
 */

public interface TypeRepository extends JpaRepository<Type, Long> {
    List<Type> findByTypeParentId(Long parentId);

    Type findByTypeName(String typename);

    void deleteByTypeParentId(Long id);

    Type findByTypeIdAndTypeParentId(Long typeId, Long ParentId);
}
