package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:49
 */

public interface CommentRepository extends JpaRepository<Comment,Long>,JpaSpecificationExecutor<Comment> {
}
