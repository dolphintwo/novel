package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Consume;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.ui.Model;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:49
 */

public interface ConsumeRepository extends JpaRepository<Consume,Long> {
    Consume findByUserId(Long userId);
    List<Consume> findByConsumeTypeAndUserIdOrderByConsumeDateDesc(String string,Long userId);
    List<Consume> findByUserIdAndConsumeObjectIdAndConsumeType(Long userId,Long objectId,String consumeType);
    @Query("select p from Consume p where ( p.consumeType=?1 or p.consumeType=?2 ) and p.isShow=?3 and p.userId =?4 order by ?#{#sort}")
    List<Consume> getUserComsumes(String consumeType, String consumeTyp, int isShow,Long userId, Sort sort);
    List <Consume> findByConsumeTypeAndUserId(String consumeType,Long userId);
}
