package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:41
 */

public interface ActivityRepository extends JpaRepository<Activity,Long> {
    Activity getActivityByBookId(long bookId);
}
