package com.dinphin.site.novel.repository;

import com.dinphin.site.novel.entity.SensitiveWord;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Desc:
 * <p>
 * <p>
 * Created by dd on 2018/9/5 14:50
 */

public interface SensitiveWordRepository extends JpaRepository<SensitiveWord, Long>, JpaSpecificationExecutor<SensitiveWord> {
    void deleteByWordId(Long wordId);
    List<SensitiveWord> findSensitiveWordsBySensitiveWordLike(String sensitiveWord);
}
